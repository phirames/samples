<?php

namespace Modules\Correspondence\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use Modules\Correspondence\Import\Batches\IncomingCorrespondenceBatch;

class CorrespondenceBulkImport implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Queueable;

    protected $config;

    protected $table;

    protected $limit;

    protected $offset;

    /**
     * CorrespondenceBulkImport constructor.
     *
     * @param array $config
     * @param string $table
     * @param int $limit
     * @param int $offset
     */
    public function __construct(array $config, string $table, int $limit, int $offset)
    {
        $this->config = $config;
        $this->table = $table;
        $this->limit = $limit;
        $this->offset = $offset;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $batch  = new IncomingCorrespondenceBatch($this->config, $this->table);

        $batch->importBulk($this->limit, $this->offset);
    }
}
