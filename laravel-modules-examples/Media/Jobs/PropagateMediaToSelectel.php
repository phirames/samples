<?php

namespace Modules\Media\Jobs;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Bus\Queueable;
use App\Repositories\MediaRepository;

class PropagateMediaToSelectel implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, Queueable;

    /**
     * @var int
     */
    protected $id;
    /**
     * @var bool
     */
    protected $removeOriginal;

    public $tries = 2;

    public function __construct(int $id, bool $removeOriginal = false)
    {
        $this->id = $id;
        $this->removeOriginal = $removeOriginal;
    }

    public function handle(MediaRepository $repo)
    {
        $originalMedia = $repo->find($this->id);

        // TODO check if the file already exists on selectel

        if ($model = $originalMedia->model) {
            $model->addMedia($originalMedia->getPath())
                ->toMediaCollection($originalMedia->collection_name, 'selectel');
        }

        if ($this->removeOriginal) {
            $originalMedia->delete();
        }

    }
}
