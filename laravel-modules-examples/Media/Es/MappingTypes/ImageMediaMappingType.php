<?php

namespace Modules\Media\Es\MappingTypes;

use Phirames\LaraElastic\MappingTypes\MappingTypeInterface;

class ImageMediaMappingType implements MappingTypeInterface
{
    public static function name(): string
    {
        return 'media_image';
    }

    public static function properties(): array
    {
        return [];
    }
}