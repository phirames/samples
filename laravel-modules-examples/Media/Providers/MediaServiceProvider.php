<?php

namespace Modules\Media\Providers;

use App\Services\Registries\EntityRegistry;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\ServiceProvider;
use Modules\Media\Console\MoveMediaToSelectel;
use Modules\Media\Console\PerformHealthCheck;
use Modules\Media\Entities\AudioAlbum;
use Modules\Media\Entities\AudioMedia;
use Modules\Media\Entities\DocMedia;
use Modules\Media\Entities\ImageMedia;
use Modules\Media\Entities\ImageMediaGallery;
use Modules\Media\Entities\VideoMedia;
use Modules\Media\GraphQL\Mutations\Audio\MediaAudioCreate;
use Modules\Media\GraphQL\Mutations\Audio\MediaAudioUpdate;
use Modules\Media\GraphQL\Queries\AudioCollectionQuery;
use Modules\Media\GraphQL\Queries\DocsCollectionQuery;
use Modules\Media\GraphQL\Queries\MediaAudioQuery;
use Modules\Media\GraphQL\Queries\MediaCollectionQuery;
use Modules\Media\GraphQL\Queries\MediaImageGalleryQuery;
use Modules\Media\GraphQL\Queries\MediaQuery;
use Modules\Media\GraphQL\Types\Input\MediaAudioCreateInput;
use Modules\Media\GraphQL\Types\Input\MediaAudioUpdateInput;
use Modules\Media\GraphQL\Types\MediaAudioCollection;
use Modules\Media\GraphQL\Types\MediaAudioType;
use Modules\Media\GraphQL\Types\MediaCollection;
use Modules\Media\GraphQL\Types\MediaDocsCollection;
use Modules\Media\GraphQL\Types\MediaImageGalleryType;
use Modules\Media\GraphQL\Types\MediaImagesCollectionType;
use Modules\Media\Es\Documents\AudioMediaDocModel;
use Modules\Media\Es\Documents\DocumentMediaDocModel;
use Modules\Media\Es\Documents\ImageMediaDocModel;
use Modules\Media\Es\MappingTypes\AudioMediaMappingType;
use Modules\Media\Es\MappingTypes\DocumentMediaMappingType;
use Modules\Media\Es\MappingTypes\ImageMediaMappingType;
use Modules\Media\Es\Query\MediaQueryProcessor;
use Modules\Media\GraphQL\Queries\ImagesCollectionQuery;
use Modules\Media\GraphQL\Queries\MediaDocQuery;
use Modules\Media\GraphQL\Queries\MediaImageQuery;
use Modules\Media\GraphQL\Types\MediaDocType;
use Modules\Media\GraphQL\Types\MediaImageType;
use Modules\Media\GraphQL\Types\Interfaces\MediaContent;
use Modules\Media\Repositories\AudioAlbumMediaRepository;
use Modules\Media\Repositories\AudioMediaRepository;
use Modules\Media\Repositories\DocMediaRepository;
use Modules\Media\Repositories\ImageMediaGalleryRepository;
use Modules\Media\Repositories\ImageMediaRepository;
use Modules\Media\GraphQL\Types\MediaType;
use Modules\Media\Repositories\VideoMediaRepository;

class MediaServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *Search
     * @var bool
     */
    protected $defer = false;

    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerRepo();
        $this->registerFactories();
        $this->app->make(EntityRegistry::class)
                  ->add(ImageMedia::class)
                  ->add(ImageMediaGallery::class)
                  ->add(AudioMedia::class)
                  ->add(AudioAlbum::class)
                  ->add(DocMedia::class)
                  ->add(VideoMedia::class);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {

        $this->commands([
            MoveMediaToSelectel::class,
            PerformHealthCheck::class,
        ]);
        $this->registerGraphQL();
        $this->registerEs();
    }

    public function registerEs()
    {
        $laraElastic = $this->app->make(\Phirames\LaraElastic\LaraElastic::class);
        $index = $laraElastic->indices()
            ->registry()
            ->getIndex(\App\Es\Indices\EgwIndex::name());
        $index->addMapping(DocumentMediaMappingType::name(), DocumentMediaMappingType::properties());
        $index->addMapping(AudioMediaMappingType::name(), AudioMediaMappingType::properties());
        $index->addMapping(ImageMediaMappingType::name(), ImageMediaMappingType::properties());

        $docRegistry = $laraElastic->docs()->registry();
        $docRegistry->add(DocumentMediaDocModel::class);
        $docRegistry->add(AudioMediaDocModel::class);
        $docRegistry->add(ImageMediaDocModel::class);

        $laraElastic->query()->addProcessor(MediaQueryProcessor::class);
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {

        $this->publishes([
            __DIR__.'/../Config/config.php' => config_path('media.php'),
        ], 'config');
        $this->mergeConfigFrom(
            __DIR__.'/../Config/config.php', 'media'
        );
    }

    protected function registerGraphQL()
    {
        \GraphQL::addSchema('default', [
            'query' => [
                'mediaImage' => MediaImageQuery::class,
                'mediaImageCollection' => ImagesCollectionQuery::class,
                'mediaDoc' => MediaDocQuery::class,
                'mediaDocsCollection' => DocsCollectionQuery::class,
                'mediaAudio' => MediaAudioQuery::class,
                'mediaAudioCollection' => AudioCollectionQuery::class,
                'mediaImageGallery' => MediaImageGalleryQuery::class,
                'media' => MediaQuery::class,
                'mediaCollection' => MediaCollectionQuery::class,
            ],
            'mutation' => [

            ]
        ]);
        \GraphQL::addSchema('secret', [
            'query' => [
            ],
            'mutation' => [
                'mediaAudioCreate' => MediaAudioCreate::class,
                'mediaAudioUpdate' => MediaAudioUpdate::class,
            ]
        ]);

        // Register graphQL types
        \GraphQL::addType(MediaContent::class, 'MediaContentInterface');
        \GraphQL::addType(MediaImageType::class, 'MediaImage');
        \GraphQL::addType(MediaImagesCollectionType::class, 'MediaImagesCollection');
        \GraphQL::addType(MediaImageGalleryType::class, 'MediaImageGallery');
        \GraphQL::addType(MediaDocType::class, 'MediaDoc');
        \GraphQL::addType(MediaDocsCollection::class, 'MediaDocsCollection');
        \GraphQL::addType(MediaAudioType::class, 'MediaAudio');
        \GraphQL::addType(MediaAudioCollection::class, 'MediaAudioCollection');
        // Media library types
        \GraphQL::addType(MediaType::class, 'Media');
        \GraphQL::addType(MediaCollection::class, 'MediaCollection');
        // Input Media types
        \GraphQL::addType(MediaAudioCreateInput::class, 'MediaAudioCreateInput');
        \GraphQL::addType(MediaAudioUpdateInput::class, 'MediaAudioUpdateInput');

    }
    protected function registerRepo()
    {
        $this->app->bind(ImageMediaGalleryRepository::class, function ($app) {
            return new ImageMediaGalleryRepository($app);
        });
        $this->app->bind(ImageMediaRepository::class, function ($app) {
            return new ImageMediaRepository($app);
        });
        $this->app->bind(DocMediaRepository::class, function ($app) {
            return new DocMediaRepository($app);
        });
        $this->app->bind(AudioAlbumMediaRepository::class, function ($app) {
            return new AudioAlbumMediaRepository($app);
        });
        $this->app->bind(AudioMediaRepository::class, function ($app) {
            return new AudioMediaRepository($app);
        });
        $this->app->bind(VideoMediaRepository::class, function ($app) {
            return new VideoMediaRepository($app);
        });
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = base_path('resources/views/modules/media');

        $sourcePath = __DIR__.'/../Resources/views';

        $this->publishes([
            $sourcePath => $viewPath
        ]);

        $this->loadViewsFrom(array_merge(array_map(function ($path) {
            return $path . '/modules/media';
        }, \Config::get('view.paths')), [$sourcePath]), 'media');
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = base_path('resources/lang/modules/media');

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, 'media');
        } else {
            $this->loadTranslationsFrom(__DIR__ .'/../Resources/lang', 'media');
        }
    }

    /**
     * Register an additional directory of factories.
     * @source https://github.com/sebastiaanluca/laravel-resource-flow/blob/develop/src/Modules/ModuleServiceProvider.php#L66
     */
    public function registerFactories()
    {
        if (! app()->environment('production')) {
            app(Factory::class)->load(__DIR__ . '/../Database/Factories');
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
