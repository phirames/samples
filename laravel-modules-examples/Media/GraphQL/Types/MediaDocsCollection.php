<?php

namespace Modules\Media\GraphQL\Types;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class MediaDocsCollection extends GraphQLType
{
    protected $attributes = [
        'name' => 'MediaDocsCollection',
        'description' => 'Collection of media documents'
    ];

    public function fields()
    {
        return [
            'total' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Total items'
            ],
            'offset' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Items offset. How many items skipped'
            ],
            'size' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'Items size. How many items are taken'
            ],
            'items' => [
                'type' => Type::listOf(\GraphQL::type('MediaDoc')),
                'description' => 'Content interface'
            ],
        ];
    }

    public function interfaces() {
        return [
            GraphQL::type('CollectionInterface')
        ];
    }
}

