<?php

namespace Modules\Media\GraphQL\Queries;

use App\GraphQL\Queries\AbstractContentCollectionQuery;
use App\Repositories\Presenters\GqlCollectionPresenter;
use GraphQL;
use League\Fractal\TransformerAbstract;
use App\Repositories\Criteria\GqlContentCollectionsArgs as ArgsCriteria;
use App\Repositories\MediaRepository;
use Modules\Media\Repositories\Transformers\MediaFileTransformer;
use Prettus\Repository\Contracts\PresenterInterface;

class MediaCollectionQuery extends AbstractContentCollectionQuery
{
    protected $attributes = [
        'name' => 'mediaCollection'
    ];

    public function type()
    {
        return GraphQL::type('MediaCollection');
    }

    public function getRepository()
    {
        return app(MediaRepository::class);
    }

    public function getItemTransformer(): TransformerAbstract
    {
        return new MediaFileTransformer();
    }

    public function getCollectionPresenter(): PresenterInterface
    {
        $presenter = new GqlCollectionPresenter($this->getItemTransformer());
        return  $presenter;
    }

    public function resolve($root, $args)
    {
        $repo = $this->getRepository();
        $repo->pushCriteria(new ArgsCriteria(collect($args)));
        $repo->setPresenter($this->getCollectionPresenter());

        $result = $repo->paginateAdvanced(['*'], $args['take'], $args['skip']);

        return $result;

    }


}
