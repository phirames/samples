<?php

namespace Modules\Media\GraphQL\Queries;

use GraphQL;
use Modules\Media\Entities\ImageMedia as Entity;
use Modules\Media\Repositories\Criteria\MediaFile;
use Modules\Media\Repositories\Criteria\MoreImagesFromGallery;
use Modules\Media\Repositories\Presenters\ImageMediaPresenter;

class MediaImageQuery extends AbstractMediaTypeQuery
{
    protected $attributes = [
        'name' => 'mediaImage'
    ];

    public function type()
    {
        return GraphQL::type('MediaImage');
    }

    public function getPresenter()
    {
        return new ImageMediaPresenter();
    }

    public function getRepository()
    {
        return app(\Modules\Media\Repositories\ImageMediaRepository::class);
    }

    public function getMediaCollectionName(): string
    {
        return (new Entity())->getMediaCollectionName();
    }

    public function resolve($root, $args)
    {
        $results =  $this->getRepository()
                         ->with('mediaFile')
                         ->pushCriteria(new MediaFile($this->getMediaCollectionName()))
                         ->pushCriteria(new MoreImagesFromGallery())
                         ->setPresenter($this->getPresenter())
                         ->find($args['id']);

        return $results;
    }

}
