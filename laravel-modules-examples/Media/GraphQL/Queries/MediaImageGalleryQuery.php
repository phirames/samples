<?php

namespace Modules\Media\GraphQL\Queries;

use GraphQL;
use Modules\Media\Entities\ImageMedia as Entity;
use Modules\Media\Repositories\Criteria\ImageGallery\WithImages;
use Modules\Media\Repositories\Presenters\ImageMediaGalleryPresenter;
use GraphQL\Type\Definition\Type;

class MediaImageGalleryQuery extends AbstractMediaTypeQuery
{
    protected $attributes = [
        'name' => 'mediaImageGallery'
    ];

    public function type()
    {
        return GraphQL::type('MediaImageGallery');
    }

    public function getPresenter()
    {
        return new ImageMediaGalleryPresenter();
    }

    public function getRepository()
    {
        return app(\Modules\Media\Repositories\ImageMediaGalleryRepository::class);
    }

    public function getMediaCollectionName(): string
    {
        return (new Entity())->getMediaCollectionName();
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' =>  Type::nonNull(Type::int())
            ],
            'imagesLimit' => [
                'name'      => 'imagesLimit',
                'type'      =>  Type::int(),
                'defaultValue'   => 25
            ],
            'imagesOffset' => ['name' => 'imagesOffset', 'type' => Type::int(), 'defaultValue' => 0],

        ];
    }

    public function resolve($root, $args)
    {
        $results =  $this->getRepository()
                         ->pushCriteria(new WithImages($args['imagesLimit'], $args['imagesOffset']))
                         ->setPresenter($this->getPresenter())
                         ->find($args['id']);

        return $results;
    }

}
