<?php

return [
    'name' => 'Media',
    'references' => [
        'images'    => \Modules\Media\Entities\ImageMedia::class,
        'audios'    => \Modules\Media\Entities\AudioMedia::class,
        'videos'    => \Modules\Media\Entities\VideoMedia::class,
        'documents' => \Modules\Media\Entities\DocMedia::class,
    ],
];
