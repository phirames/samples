<?php

namespace Modules\Media\Http\Controllers;

use App\Repositories\Criteria\RelationsRequestCriteria;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Media\Repositories\ImageMediaGalleryRepository;
use Modules\Media\Repositories\ImageMediaRepository;
use Modules\Media\Repositories\Presenters\ImageMediaGalleryPresenter;
use Modules\Media\Repositories\Presenters\ImageMediaPresenter;

class MediaImageGalleryController extends Controller
{
    /**
     * @var ImageMediaGalleryRepository
     */
    protected $galleryRepo;

    /**
     * @var ImageMediaRepository
     */
    protected $imageRepo;

    /**
     * MediaImageGalleryController constructor.
     *
     * @param ImageMediaGalleryRepository $galleryRepo
     * @param ImageMediaRepository $imageRepo
     */
    public function __construct(ImageMediaGalleryRepository $galleryRepo, ImageMediaRepository $imageRepo)
    {
        $this->galleryRepo = $galleryRepo;
        $this->imageRepo = $imageRepo;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('media::image-gallery.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('media::image-gallery::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(int $id)
    {
        $gallery = $this->galleryRepo->find($id);

        return view('media::image-gallery.show', [
            'gallery' => $gallery,
            'images' => $gallery->images()->paginate(100),

        ]);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function dataList(Request $request)
    {
        $relations = $request->get('with', []);
        $limit = $request->get('limit', 25);

        if ($searchString = $request->get('search_string')) {

            $entities = $this->galleryRepo
                ->load($relations)
                ->setPresenter($this->getGalleryPresenter())
                ->paginatedSearch($searchString, function($client, $query, $params) {
                    $params['body']['query'] = [
                        'multi_match' => [
                            'query'  => $query,
                            'fields' => ['title']
                        ],
                    ];
                    return $client->search($params);
                }, $limit);

        } else {

            $entities = $this->galleryRepo
                ->pushCriteria(new RelationsRequestCriteria())
                ->setPresenter($this->getGalleryPresenter())
                ->paginate($limit);
        }

        return \Response::json($entities, 206);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     * @throws \Exception
     */
    public function dataItem(Request $request, int $id)
    {
        $location = $this->galleryRepo
            ->with($relations = $request->get('with', []))
            ->setPresenter($this->getGalleryPresenter())
            ->find($id);

        return \Response::json($location, 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('media::image-gallery.edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    protected function getGalleryPresenter()
    {
        return new ImageMediaGalleryPresenter();
    }

    protected function getImagePresenter()
    {
        return new ImageMediaPresenter();
    }
}
