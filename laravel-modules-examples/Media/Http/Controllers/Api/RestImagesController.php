<?php

namespace Modules\Media\Http\Controllers\Api;

use App\Repositories\Criteria\RelationsRequestCriteria;
use App\Repositories\Criteria\SortById;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Media\Entities\ImageMedia;
use Modules\Media\Repositories\ImageMediaRepository;
use Modules\Media\Repositories\Presenters\ImageMediaPresenter;

class RestImagesController extends Controller
{
    /**
     * @var ImageMediaRepository
     */
    private $repo;

    public function __construct(ImageMediaRepository $repository)
    {
        $this->repo = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request )
    {
        $relations = $request->get('with', []);

        if ($searchString = $request->get('search_string')) {

            $entities = $this->repo
                ->load($relations)
                ->setPresenter($this->createPresenter())
                ->paginatedSearch($searchString, function($client, $query, $params) {
                    $params['body']['query'] = [
                        'multi_match' => [
                            'query'  => $query,
                            'fields' => ['title']
                        ],
                    ];
                    return $client->search($params);
                });

        } else {

            $entities = $this->repo
                ->pushCriteria(new RelationsRequestCriteria())
                ->pushCriteria(new SortById())
                ->setPresenter($this->createPresenter())
                ->paginate();
        }

        return response()->json($entities, 206);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('media::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'         => 'required|max:255',
            'body'          => 'string',
            'media'         => 'required|file|max:20480|image:jpeg,png,bmp,gif,svg',
            'gallery_ids'   => 'array',
            'gallery_ids.*' => 'integer',
            'references'    => 'json', // 'references' should be passed as a regular POST from-data. E.g.: `references:{"id":2}`
            'terms'         => 'json'
        ]);

        $media = ImageMedia::create($request->all());

        $media->addMediaFromRequest('media')
              ->toMediaCollection($media->getMediaCollectionName());

        $media->galleries()->sync($request->get('gallery_ids', []));

        $media->syncRefsRequest($request);
        $media->syncTermsRequest($request);

        if ($relations = $request->get('with', [])) {
            $media->load($relations);
        }

        return response()->json($this->createPresenter()->present($media), 201);
    }

    /**
     * Show the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function show(Request $request, int $id)
    {
        $relations = $request->get('with', []);

        $media = $this->repo
            ->with($relations)
            ->setPresenter($this->createPresenter())
            ->find($id);

        return response()->json($media, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        return view('media::edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            'title'         => 'required|max:255',
            'body'          => 'string',
            'gallery_ids'   => 'array',
            'gallery_ids.*' => 'integer',
            'references'    => 'json',
            'terms'         => 'json'
        ]);

        $entity = $this->repo->find($id);

        $this->authorize('update', $entity);

        $entity->update([
            'title' => $request->get('title'),
            'body'  => $request->get('body'),
        ]);
        $entity->galleries()->sync($request->get('gallery_ids', []));


        $entity->syncRefsRequest($request);
        $entity->syncTermsRequest($request);

        if ($relations = $request->get('with', [])) {
            $entity->load($relations);
        }

        return response()->json($this->createPresenter()->present($entity), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(int $id)
    {
        $entity = $this->repo->find($id);

        $this->authorize('delete', $entity);

        $entity->delete();

        return response()->json($entity, 204);
    }

    /**
     * Updates media file nested in the content media type entity
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function updateMediaFile(Request $request, int $id)
    {
        // Make sure new file is loaded
        $request->validate([
            'media' => 'required|file|max:20480|image:jpeg,png,bmp,gif,svg',
        ]);

        $media = $this->repo->with('media')->find($id);

        $this->authorize('update', $media);

        // Delete existing media file
        $media->mediaFile->delete();

        //Add new media file
        $media->addMediaFromRequest('media')
              ->toMediaCollection($media->getMediaCollectionName());

        $media->load(['mediaFile', 'references']);

        return response()->json($this->createPresenter()->present($media), 201);
    }

    /**
     * @return ImageMediaPresenter
     * @throws \Exception
     */
    public function createPresenter()
    {
        return new ImageMediaPresenter();
    }
}
