<?php

namespace Modules\Media\Http\Controllers\Api;

use App\Repositories\Criteria\SearchRequestByTitle;
use App\Repositories\Criteria\SortById;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Media\Entities\AudioAlbum;
use Modules\Media\Entities\AudioMedia as Entity;
use Modules\Media\Repositories\AudioMediaRepository;
use Modules\Media\Repositories\Presenters\AudioMediaPresenter;

class RestAudioController extends Controller
{
    /**
     * @var AudioMediaRepository
     */
    private $repo;

    public function __construct(AudioMediaRepository $repository)
    {
        $this->repo = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        //Takes 'mediaFile', 'album', 'references', 'terms'
        $relations = $request->get('with', []);

        $entity = $this->repo
            ->pushCriteria(new SearchRequestByTitle($request))
            ->pushCriteria(new SortById())
            ->with($relations)
            ->setPresenter($this->getPresenter())
            ->orderBy('id', 'asc')
            ->paginate(25);

        return response()->json($entity, 206);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('media::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'       => 'required|max:255',
            'body'        => 'string',
            'mediaFile'   => 'required|file|max:20480',
            'album_id'    => 'integer|exists:media_audio_album,id',
            'terms'       => 'json'
        ]);

        $entity = Entity::create([
            'title'      => $request->get('title'),
            'body'       => $request->get('body'),
            'album_id'   => $request->get('album_id', null),
            'references' => $request->get('references'),
        ]);

        $entity->addMediaFromRequest('mediaFile')
              ->toMediaCollection($entity->getMediaCollectionName());

        if ($album = $request->get('album_id')) {
            $entity->album()->associate(AudioAlbum::find($album));
        }
        $entity->save();

        $entity->syncRefsRequest($request);
        $entity->syncTermsRequest($request);

        //Takes 'mediaFile', 'album', 'references', 'terms'
        if ($relations = $request->get('with', [])) {
            $entity->load($relations);
        }

        return response()->json($this->getPresenter()->present($entity), 201);
    }

    /**
     * Show the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function show(Request $request, int $id)
    {
        //Takes 'mediaFile', 'album', 'references', 'terms'
        $relations = $request->get('with', []);

        $entity = $this->repo
            ->with($relations)
            ->setPresenter($this->getPresenter())
            ->find($id);

        return response()->json($entity, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        return view('media::edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            'title'       => 'required|max:255',
            'body'        => 'string',
            'album_id'    => 'integer|exists:media_audio_album,id',
            'terms'       => 'json'
        ]);

        $entity = $this->repo->find($id);

        $this->authorize('update', $entity);

        $entity->update([
            'title' => $request->get('title'),
            'body'  => $request->get('body'),
            'references' => $request->get('references')
        ]);

        $album_id = $request->get('album_id');
        if (!is_null($album_id) && $entity->album_id === (int) $album_id) {
            $entity->album()->associate(AudioAlbum::find($album_id));
        }

        $entity->save();

        $entity->syncRefsRequest($request);
        $entity->syncTermsRequest($request);

        //Takes 'mediaFile', 'album', 'references', 'terms'
        if ($relations = $request->get('with', [])) {
            $entity->load($relations);
        }

        return response()->json($this->getPresenter()->present($entity), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(int $id)
    {
        $entity = $this->repo->find($id);

        $this->authorize('delete', $entity);

        $entity->delete();

        return response()->json($entity, 204);
    }

    /**
     * Updates media file nested in the content media type entity
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function updateMediaFile(Request $request, int $id)
    {
        // Make sure new file is loaded
        $request->validate([
            'mediaFile'   => 'required|file|max:20480',
        ]);

        $media = $this->repo->with('mediaFile')->find($id);

        $this->authorize('update', $media);

        // Delete existing media file
        if ($media->mediaFile) {
            $media->mediaFile->delete();
        }

        //Add new media file
        $media->addMediaFromRequest('mediaFile')
              ->toMediaCollection($media->getMediaCollectionName());

        $media->load('mediaFile');

        return response()->json($this->getPresenter()->present($media), 201);
    }

    /**
     * @return AudioMediaPresenter
     * @throws \Exception
     */
    public function getPresenter()
    {
        return new AudioMediaPresenter();
    }
}
