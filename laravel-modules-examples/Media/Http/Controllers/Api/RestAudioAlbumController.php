<?php

namespace Modules\Media\Http\Controllers\Api;

use App\Repositories\Criteria\SearchRequestByTitle;
use App\Repositories\Criteria\SortById;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Media\Entities\AudioAlbum as Entity;
use Modules\Media\Entities\AudioAlbum;
use Modules\Media\Repositories\AudioAlbumMediaRepository as Repository;
use Modules\Media\Repositories\AudioMediaRepository;
use Modules\Media\Repositories\Criteria\ByMediaAlbum;
use Modules\Media\Repositories\Presenters\AudioAlbumMediaPresenter as Presenter;
use Modules\Media\Repositories\Presenters\AudioMediaPresenter;

/**
 * REST API controller for audio albums
 *
 * Class RestAudioAlbumController
 * @package Modules\Media\Http\Controllers\Api
 */
class RestAudioAlbumController extends Controller
{
    /**
     * @var Repository
     */
    private $repo;

    public function __construct(Repository $repository)
    {
        $this->repo = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        //Takes 'cover', 'tracks', 'references'
        $relations = $request->get('with', []);

        $media = $this->repo
            ->pushCriteria(new SearchRequestByTitle($request))
            ->pushCriteria(new SortById())
            ->with($relations)
            ->setPresenter($this->getPresenter())
            ->orderBy('id', 'asc')
            ->paginate(25);

        return response()->json($media, 206);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('media::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'cover' => 'required|file|max:2048|image:jpeg,png,bmp,gif,svg',
            'references' => 'json'
        ]);
        $album = Entity::create($request->all());

        //Add new media file
        $album->addMediaFromRequest('cover')
                ->toMediaCollection(Entity::COVER_MEDIA_COLLECTION_NAME);

        $album->syncRefsRequest($request);

        //Takes 'cover', 'tracks', 'references'
        if ($relations = $request->get('with', [])) {
            $album->load($relations);
        }

        return response()->json($this->getPresenter()->present($album), 201);
    }

    /**
     * Show the specified resource.
     *
     * @param  Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function show(Request $request, int $id)
    {
        //Takes 'cover', 'tracks', 'references'
        $relations = $request->get('with', []);

        $trackLimit = $request->get('trackLimit', 10);

        $album = $this->repo
            ->with(array_merge($relations, ['tracks' => function($q) use($trackLimit) {
                return $q->take($trackLimit);
            }]))
            ->setPresenter($this->getPresenter())
            ->find($id);

        return response()->json($album, 200);
    }

    /**
     * Show the specified resource.
     *
     * @param  Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function showTracks(Request $request, int $id)
    {
        $imageRepo = app(Repository::class);
        $album = $imageRepo->with('tracks')
            ->setPresenter(new Presenter())
            ->paginate();

        $tracks = app(AudioMediaRepository::class)
            ->pushCriteria(new SearchRequestByTitle($request))
            ->pushCriteria(new ByMediaAlbum($id))
            ->with(['mediaFile'])
            ->setPresenter(new AudioMediaPresenter())
            ->orderBy('id', 'asc')
            ->paginate(25);

        return response()->json($tracks, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        return view('media::edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            'title' => 'required|max:255',
            'references' => 'json'
        ]);

        $album = $this->repo->find($id);

        $this->authorize('update', $album);

        $album->update($request->all());

        $album->syncRefsRequest($request);

        //Takes 'cover', 'tracks', 'references'
        if ($relations = $request->get('with', [])) {
            $album->load($relations);
        }

        return response()->json($this->getPresenter()->present($album), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(int $id)
    {
        $entity = $this->repo->find($id);

        $this->authorize('delete', $entity);

        $entity->delete();

        return response()->json($entity, 204);
    }

    /**
     * Updates media file nested in the content media type entity
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function updateCover(Request $request, int $id)
    {
        // Make sure new file is loaded
        $request->validate([
            'cover' => 'required|file|max:2048|image:jpeg,png,bmp,gif,svg',
        ]);

        $album = $this->repo
            ->find($id);

        $this->authorize('update', $album);

        // Delete existing media file
        $album->cover->delete();

        //Add new media file
        $album->addMediaFromRequest('cover')
              ->toMediaCollection(AudioAlbum::COVER_MEDIA_COLLECTION_NAME);

        $album->load('cover');

        return response()->json($this->getPresenter()->present($album), 201);
    }

    /**
     * @return Presenter
     * @throws \Exception
     */
    public function getPresenter()
    {
        return new Presenter();
    }
}
