<?php

namespace Modules\Media\Repositories\Presenters;

use Modules\Media\Repositories\Transformers\MediaImageTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class ImageMediaPresenter extends FractalPresenter
{

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new MediaImageTransformer();
    }
}
