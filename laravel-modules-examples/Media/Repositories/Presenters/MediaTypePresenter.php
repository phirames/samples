<?php

namespace Modules\Media\Repositories\Presenters;

use Modules\Media\Repositories\Transformers\MediaTypeTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class MediaTypePresenter extends FractalPresenter
{

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new MediaTypeTransformer();
    }
}
