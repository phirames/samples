<?php

namespace Modules\Media\Repositories\Presenters;

use Modules\Media\Repositories\Transformers\ImageMediaGalleryTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class ImageMediaGalleryPresenter extends FractalPresenter
{

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ImageMediaGalleryTransformer();
    }
}
