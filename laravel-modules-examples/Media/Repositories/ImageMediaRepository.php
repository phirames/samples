<?php

namespace Modules\Media\Repositories;


use App\Repositories\BaseAppRepository;
use Modules\Media\Entities\ImageMedia;

class ImageMediaRepository extends BaseAppRepository
{
    public function model()
    {
        return ImageMedia::class;
    }

}