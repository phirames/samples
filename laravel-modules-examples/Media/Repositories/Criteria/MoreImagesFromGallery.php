<?php

namespace Modules\Media\Repositories\Criteria;

use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class MoreImagesFromGallery implements CriteriaInterface {

    public function apply($model, RepositoryInterface $repository)
    {
        return $model->with(['galleries']);
    }

}
