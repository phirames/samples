<?php

namespace Modules\Media\Repositories\Criteria;

use Modules\Media\Entities\Interfaces\MediaEntityInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

class MediaFile implements CriteriaInterface {

    protected $collectionName;

    public function __construct(string $collectionName = null)
    {
        $this->collectionName = $collectionName;
    }

    public function apply($model, RepositoryInterface $repository)
    {

        $collectionName = $this->getMediaCollectionName($model);

        return $model->with(['media' => function($q) use($collectionName) {
            $q->where('collection_name', '=', $collectionName);
        }]);
    }

    protected function getMediaCollectionName($model) {

       if (!$this->collectionName && $model instanceof MediaEntityInterface) {
            return $model->getMediaCollectionName();
        }

        return $this->collectionName;
    }

}
