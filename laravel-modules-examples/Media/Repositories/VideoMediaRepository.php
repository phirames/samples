<?php

namespace Modules\Media\Repositories;

use App\Repositories\BaseAppRepository;
use Modules\Media\Entities\VideoMedia;

class VideoMediaRepository extends BaseAppRepository
{
    public function model()
    {
        return VideoMedia::class;
    }

}