<?php

namespace Modules\Media\Repositories\Transformers;

use App\Repositories\Transformers\EloquentTransformer;
use App\Repositories\Transformers\Traits\ReferenceTransformerTrait;
use \App\Entities\Interfaces\ContentInterface;
use App\Repositories\Transformers\Traits\TermTransformerTrait;
use Illuminate\Database\Eloquent\Model as Entity;
use Modules\Media\Repositories\Transformers\ImageMediaGalleryTransformer;
use Modules\Media\Repositories\Transformers\MediaFileIncludeTrait;
use Nwidart\Modules\Collection;

class MediaImageTransformer extends EloquentTransformer
{

    use MediaFileIncludeTrait;
    use ReferenceTransformerTrait;
    use TermTransformerTrait;

    protected $defaultIncludes = [
        'mediaFile',
        'galleries',
        'references',
        'loadedReferences',
        'terms'
    ];

    public function transform(Entity $entity)
    {
        $route = ($entity instanceof ContentInterface) ? $entity->route([], false) : null;

        return [
            'id'                => (int) $entity->id,
            'title'             => $entity->title,
            'body'              => $entity->body,
            'thumbnail'         => $entity->thumb_site_path,
            'route'             => $route,
            'date'              => $entity->date,
        ];
    }

    public function includeGalleries(Entity $entity)
    {
        $galleries = $entity->relationLoaded('galleries')
            ? $entity->getRelation('galleries')
            : new Collection();

        return $this->collection($galleries, new ImageMediaGalleryTransformer());
    }

}