<?php

namespace Modules\Media\Entities;

use App\Entities\Interfaces\ReferenceInterface;
use App\Entities\Traits\References;
use Modules\Media\GraphQL\Types\MediaVideoType;
use Modules\Taxonomy\Entities\Traits\TaxonomyTrait;
use Spatie\MediaLibrary\Media;
use Modules\Media\Es\Documents\VideoMediaDocModel as EsDoc;
use Modules\Media\Entities\Media as BasicMedia;

class VideoMedia extends BasicMedia implements ReferenceInterface
{
    use References, TaxonomyTrait;

    protected $fillable = [
        'title', 'body', 'taxonomy', 'media_data', 'metadata', 'user_id'
    ];

    protected $table = 'media_video';

    protected $casts = [
        'taxonomy' => 'json',
        'references' => 'json',
        'media_data' => 'json',
        'metadata' => 'json',
    ];

    protected $routeName = 'video.item';

    protected $contentTypeName = 'media_video';

    public function getMediaCollectionName(): string
    {
        return 'video';
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(150)
            ->sharpen(10);
    }

    public function getGqlType(): string
    {
        return (new MediaVideoType())->name;
    }

    public function toSearchableArray()
    {
        return (new EsDoc($this))->source();
    }

}
