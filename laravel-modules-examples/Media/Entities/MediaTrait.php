<?php


namespace Modules\Media\Entities;

use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Media;

trait MediaTrait
{
    use HasMediaTrait;

    /**
     *
     * Add media file from url
     *
     * @param string $url
     * @param string $mediaLibrary
     *
     * @return Media
     */
    public function addMediaFileFromUrl(string $url, string $mediaLibrary = ''): Media
    {
        return $this->addMediaFromUrl($url)->toMediaLibrary($mediaLibrary ?: $this->table);
    }

}