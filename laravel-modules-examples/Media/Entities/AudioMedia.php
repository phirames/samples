<?php

namespace Modules\Media\Entities;

use App\Entities\Interfaces\ReferenceInterface;
use App\Entities\Traits\References;
use Modules\Media\GraphQL\Types\MediaAudioType;
use Modules\Media\Entities\Media as BasicMedia;
use Modules\Media\Es\Documents\AudioMediaDocModel as EsDoc;
use Modules\Taxonomy\Entities\Traits\TaxonomyTrait;

class AudioMedia extends BasicMedia implements ReferenceInterface
{
    use References, TaxonomyTrait;

    protected $fillable = [
        'title',
        'body',
        'taxonomy',
        'media_data',
        'metadata',
        'user_id',
        'track_url'
    ];

    protected $table = 'media_audio';

    protected $casts = [
        'taxonomy' => 'object',
        'references' => 'object',
        'media_data' => 'object',
        'metadata' => 'object',
    ];

    protected $routeName = 'audio.item';

    protected $mappingName = 'media_audio';

    protected $contentTypeName = 'media_audio';

    public function getMediaCollectionName(): string
    {
        return 'media_audio';
    }

    public function album()
    {
        return $this->belongsTo(AudioAlbum::class);
    }

    public function getGqlType(): string
    {
        return (new MediaAudioType())->name;
    }

    public function toSearchableArray()
    {
        return (new EsDoc($this))->source();
    }

}
