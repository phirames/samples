<?php

namespace Modules\Flowpaper\Factories;

class PdfSourceFactory
{
    public function make($sourceFile, string $filesystem = null)
    {
        $namespace = config('flowpaper.source');
        return new $namespace($sourceFile, $filesystem);

    }
}