<?php

namespace Modules\Flowpaper\Services;


use Modules\Flowpaper\Entities\Pdf;
use Modules\Flowpaper\Services\Exceptions\PropagatorException;

/**
 * Class SelectelPropagator
 * @package Modules\Flowpaper\Services
 */
class SelectelPropagator implements PropagatorInterface
{

    const SELECTEL_DISK = 'selectel';

    /**
     * @var Pdf
     */
    protected $entry;

    protected $srcFilesystem;

    public function __construct($srcFilesystem = null)
    {
        $this->srcFilesystem = $srcFilesystem ?? config('flowpaper.processor.local.filesystem');
    }

    /**
     * @param Pdf $pdf
     *
     * @return $this
     */
    public function setEntry(Pdf $pdf)
    {
        $this->entry = $pdf;

        return $this;
    }

    /**
     * @throws \Exception
     */
    public function propagate()
    {
        if (!$this->entry) {
            throw PropagatorException::missingEntry();
        }

        if ($this->entry->status === $this->entry::STATUS_OK && $this->entry->processed_files) {

            $this->propagateFile($this->entry->processed_files->pdf);
            $this->propagateFile($this->entry->processed_files->json);
            $this->propagateFiles($this->entry->processed_files->pngs);
            $this->propagateFiles($this->entry->processed_files->swfs);

        } else {
            throw PropagatorException::unprocessedEntry($this->entry->id);
        }

        $this->entry->disk = self::SELECTEL_DISK;
        $this->entry->save();

    }

    /**
     * @param string $filePath
     */
    protected function propagateFile(string $filePath)
    {
        $file = \Storage::disk($this->srcFilesystem)
                        ->get($filePath);

        \Storage::disk(self::SELECTEL_DISK)
                ->put($filePath, $file);
    }

    /**
     * @param array $filePaths
     */
    protected function propagateFiles(array $filePaths)
    {
        foreach ($filePaths as $filePath) {
            $this->propagateFile($filePath);
        }
    }
}