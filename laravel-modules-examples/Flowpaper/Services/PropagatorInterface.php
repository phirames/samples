<?php

namespace Modules\Flowpaper\Services;


/**
 * Interface PropagatorInterface
 * @package Modules\Flowpaper\Services
 */
interface PropagatorInterface
{

    /**
     * Propagates files
     */
    public function propagate();
}