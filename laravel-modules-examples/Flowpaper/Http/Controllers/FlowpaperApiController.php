<?php

namespace Modules\Flowpaper\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Flowpaper\Processor\Exceptions\FlowpaperSource;
use Modules\Flowpaper\Factories\PdfSourceFactory;
use Modules\Flowpaper\Jobs\ProcessPdfJob;
use Modules\Flowpaper\Processor\PdfLocalProcessor;
use Modules\Flowpaper\Repositories\FlowpaperRepository;
use Modules\Flowpaper\Repositories\Presenters\ArrayPresenter;

class FlowpaperApiController extends Controller
{

    /**
     * @var FlowpaperRepository
     */
    protected $repo;

    /**
     * FlowpaperApiController constructor.
     *
     * @param FlowpaperRepository $repo
     */
    public function __construct(FlowpaperRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Return document by id or list of documents.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function documents(Request $request)
    {
        $docs = $this->repo
            ->setPresenter($this->getPresenter())
            ->paginate($request->get('limit', 25));

        return response()->json([
            'documents' => $docs,
        ]);
    }

    /**
     * Return document by id or list of documents.
     *
     * @param int $id
     *
     * @return Response
     */
    public function document(int $id)
    {
        return response()->json([
            'document' => $this->repo
                ->setPresenter($this->getPresenter())
                ->find($id)
        ]);

    }

    /**
     * Process the pdf on demand if it does not exist
     *
     * @param Request $request
     * @param PdfSourceFactory $factory
     * @param PdfLocalProcessor $processor
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDocument(Request $request, PdfSourceFactory $factory, PdfLocalProcessor $processor)
    {
        $request->validate([
            'source_file' => 'required|string|max:1024'
        ]);

        try {
            $sourceFile = $request->get('source_file');
            $source = $factory->make($sourceFile);

        } catch (FlowpaperSource $exception) {
            return response()->json( [
                'errors' => [$exception->getMessage()],
            ], 422);
        }

        $processor->setSource($source);

        // If the source document was not processed we'll try to process that in queue
        if ($processor->isUnprocessed()) {

            $job = new ProcessPdfJob($source->getSrcFilePath());
            dispatch($job)->onQueue('flowpaper-processor');

            // We set pending status to let the client know that it is in processing
            $processor->setPendingStatus();

            // Note: make sure the queue worker for 'flowpaper-processor' channel is ran
        }

        // At any case we'll present the document with current status.
        // The client is able to handle status on its own.

        return response()->json( [
            'document' => $this->getPresenter()->present($processor->document()),
        ]);

    }

    /**
     * @return ArrayPresenter
     */
    protected function getPresenter()
    {
        return new ArrayPresenter();
    }

}
