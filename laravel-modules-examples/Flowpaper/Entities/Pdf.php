<?php

namespace Modules\Flowpaper\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Flowpaper\PathGenerators\PathPatternGetter;

/**
 * Class Pdf
 * @property string disk Storage disk name
 * @property string source_file
 * @property string status
 * @property object patterns
 * @property object processed_files
 * @property object path_patterns
 *
 * @package Modules\Flowpaper\Entities
 */
class Pdf extends Model implements FlowpaperEntry
{
    const STATUS_UNPROCESSED = 'unprocessed';
    const STATUS_PENDING = 'pending';
    const STATUS_FAILED = 'failed';
    const STATUS_OK = 'ok';

    protected $fillable = [
        'source_file',
        'processed_files',
        'disk',
        'status',
    ];

    protected $table = 'pdf_processed';

    protected $casts = [
        'processed_files' => 'object',
    ];

    protected  $appends = ['path_patterns'];

    /**
     * @var PathPatternGetter
     */
    protected $patternGetter;

    public function getPatternsAttribute()
    {
        if (!$this->patternGetter) {
            $this->patternGetter = new PathPatternGetter($this);
        }

        return $this->patternGetter;
    }

    /**
     * @param $value
     *
     * @return \stdClass
     */
    public function getPathPatternsAttribute($value)
    {
        $resp = new \stdClass();
        $resp->pdf = $this->patterns->pdf();
        $resp->swf = $this->patterns->swf();
        $resp->png = $this->patterns->png();
        $resp->json = $this->patterns->json();

        return $resp;
    }

    /**
     * @return string
     */
    public function getDiskName(): string
    {
        return $this->disk;
    }

}
