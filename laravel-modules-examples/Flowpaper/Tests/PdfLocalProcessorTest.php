<?php

namespace Modules\Flowpaper\Tests;

use Modules\Flowpaper\Entities\Pdf;
use Modules\Flowpaper\Factories\PdfSourceFactory;
use Modules\Flowpaper\Processor\PdfLocalProcessor;
use Modules\Flowpaper\Processor\Source;
use Tests\CreatesApplication;
use Tests\TestCase;
use \Artisan;
use \Storage;


class PdfLocalProcessorTest extends TestCase
{

    use CreatesApplication;

    /**
     * @var PdfLocalProcessor
     */
    protected $pdfProcessor;

    /**
     * @var Source
     */
    protected $source;

    /**
     * Test pdf path
     * @var string
     */
    protected $pdfPath = '/var/www/laravel/storage/tests/pdf/4527.pdf';

    protected $pagesAmount = 2;

    public function setUp()
    {
        parent::setUp();
        Artisan::call('module:migrate', [
            'module' => 'Flowpaper'
        ]);

        $this->pdfProcessor = app(PdfLocalProcessor::class);
        $this->source = app(PdfSourceFactory::class)->make($this->pdfPath);

        // Set fake flowpaper file system disk
        $fsConfigFlowpaper = config('filesystems.disks.flowpaper');
        $fsConfigFlowpaper['root'] = storage_path('tests/fake/flowpaper');

        config(['filesystems.disks.flowpaper' => $fsConfigFlowpaper]);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testProcessPdfTest()
    {
        $this->pdfProcessor
            ->setSource($this->source)
            ->process();

        $this->assertDatabaseHas('pdf_processed', [
            'source_file' => $this->pdfPath,
        ]);
        $model = Pdf::where('source_file', '=', $this->pdfPath)->first();

        if ($model) {
            $this->assertTrue(true, 'Dummy pdf had been created');
        }

        $this->assertNotNull($processedFiles = $model->processed_files, 'File processed');

        $countSwf = isset($processedFiles->swfs) ? count($processedFiles->swfs) : 0;
        $countPng = isset($processedFiles->pngs) ? count($processedFiles->pngs) : 0;
        $jsonPath = isset($processedFiles->json) ? storage_path('tests/fake/flowpaper') . '/' . ($processedFiles->json) : 0;

        $this->assertEquals($countSwf, $this->pagesAmount, 'Correct amount of swf files was generated');
        $this->assertEquals($countPng, $this->pagesAmount, 'Correct amount of png files was generated');
        $this->assertFileExists($jsonPath, 'Json file extracted');

        $model->delete();
        $this->rmFakeStorage();

    }


    public function tearDown()
    {

    }
}