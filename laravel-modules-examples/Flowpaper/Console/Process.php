<?php

namespace Modules\Flowpaper\Console;

use Illuminate\Console\Command;
use Modules\Flowpaper\Factories\PdfSourceFactory;
use Modules\Flowpaper\Jobs\ProcessPdfJob;
use Modules\Flowpaper\Processor\PdfLocalProcessor;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class Process extends Command
{

    /**
     * @var string
     */
    protected $source;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'flowpaper:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Process source files';

    /**
     * Execute the console command.
     *
     *
     * @param PdfSourceFactory $factory
     * @param PdfLocalProcessor $processor
     *
     * @return mixed
     */
    public function handle(PdfSourceFactory $factory, PdfLocalProcessor $processor)
    {
        // Create source
        $sourcePath = $this->argument('source');
        $disk = $this->option('disk');
        $this->source = $factory->make($sourcePath, $disk);
        
        
        // If queue flag takes the place create queue task
        if ($this->option('queue')) {

            $job = new ProcessPdfJob($this->source->getSrcFilePath());
            dispatch($job)->onQueue('flowpaper-processor');

        } else {

            // Otherwise process the source instantly
            $processor->setSource($this->source)
                      ->process();
        }

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['source', InputArgument::REQUIRED, 'Url or local path of the file to process'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['queue', null, InputOption::VALUE_NONE, 'Run in queue'],
            ['disk', null, InputOption::VALUE_OPTIONAL, 'File system disk name'],
        ];
    }
}
