<?php

namespace Modules\Flowpaper\Repositories\Presenters;

use Modules\Flowpaper\Repositories\Transformers\ArrayTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class ArrayPresenter extends FractalPresenter
{

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ArrayTransformer();
    }
}
