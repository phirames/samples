<?php

namespace Modules\Flowpaper\Processor\Interfaces;


interface SourceInterface
{
    /**
     * Gets the destination file path
     */
    public function getDistFilePath(): string;

    /**
     * Gets the source file path
     */
    public function getSrcFilePath(): string;
}