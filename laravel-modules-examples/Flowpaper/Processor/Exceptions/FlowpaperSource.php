<?php

namespace Modules\Flowpaper\Processor\Exceptions;

use \Exception;

class FlowpaperSource extends Exception
{
    public static function fileDoesNotExists($source)
    {
        return new self("File '{$source}' does not exist.'" );
    }
}
