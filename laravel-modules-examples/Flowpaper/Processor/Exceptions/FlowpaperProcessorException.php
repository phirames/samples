<?php

namespace Modules\Flowpaper\Processor\Exceptions;

use \Exception;

class FlowpaperProcessorException extends Exception
{
    public static function cannotCopyFile($path)
    {
        return new self("Can not copy file '{$path}'");
    }

    public static function swfConversionFailed($message)
    {
        return new self("Conversion process was failed. Log info: \n {$message}");
    }
}
