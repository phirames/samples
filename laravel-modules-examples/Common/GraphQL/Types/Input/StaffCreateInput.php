<?php

namespace Modules\Common\GraphQL\Types\Input;


use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class StaffCreateInput extends GraphQLType
{
    protected $attributes = [
        'name' => 'StaffCreateInput',
        'description' => 'Staff input type'
    ];

    protected $inputObject = true;

    public function fields()
    {
        return [
            'title' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Person\'s title',
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'Person\'s description',
            ],
            'avatar' => [
                'type' => GraphQL::type('TempFile'),
                'description' => 'Person\'s image file'
            ],
        ];
    }
}
