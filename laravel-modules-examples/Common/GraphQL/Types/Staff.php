<?php

namespace Modules\Common\GraphQL\Types;


use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;


class Staff extends GraphQLType
{
    protected $attributes = [
        'name' => 'Staff',
        'description' => 'A staff'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of the staff entry'
            ],
            'title' => [
                'type' => Type::string(),
                'description' => 'Staff\'s title',
            ],
            'description' => [
                'type' => Type::string(),
                'description' => 'Staff\' description',
            ],
            'avatar' => [
                'type' => GraphQL::type('Media'),
                'description' => 'Avatar of the staff'
            ],
        ];
    }

}
