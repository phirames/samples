<?php

namespace Modules\Common\GraphQL\Queries;

use App\GraphQL\Queries\AbstractContentCollectionQuery;
use GraphQL;
use GraphQL\Type\Definition\Type;
use League\Fractal\TransformerAbstract;
use Modules\Common\Repositories\Transformers\StaffTransformer;
use Modules\Common\Repositories\StaffRepository;
use App\Repositories\Criteria\GqlContentCollectionsArgs as ArgsCriteria;

class StaffCollectionQuery extends AbstractContentCollectionQuery
{
    protected $attributes = [
        'name' => 'staffCollection'
    ];

    public function type()
    {
        return GraphQL::type('StaffCollectionType');
    }

    /**
     * @return StaffRepository
     */
    public function getRepository()
    {
        return app(StaffRepository::class);
    }

    /**
     * @return TransformerAbstract
     */
    public function getItemTransformer(): TransformerAbstract
    {
        return new StaffTransformer();
    }

    public function args()
    {
        return [
            'title' => ['name' => 'title', 'type' => Type::string()],
            'take' => ['name' => 'take', 'type' => Type::int(), 'defaultValue' => 25],
            'skip' => ['name' => 'skip', 'type' => Type::int(), 'defaultValue' => 0],
        ];
    }

    public function resolve($root, $args)
    {
        $repo = $this->getRepository();
        $repo->pushCriteria(new ArgsCriteria(collect($args)));
        $repo->with(['avatar']);
        $repo->setPresenter($this->getCollectionPresenter());

        $result = $repo->paginateAdvanced(['*'], $args['take'], $args['skip']);

        return $result;

    }

}
