<?php

namespace Modules\Common\GraphQL\Mutations;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use Modules\Common\Repositories\StaffRepository;
use Modules\People\Entities\Person;
use Modules\People\Repositories\PeopleRepository;
use Modules\People\Repositories\Presenters\PersonPresenter;
use Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class StaffDelete extends Mutation
{

    protected $attributes = [
        'name' => 'staffDelete'
    ];

    public function type()
    {
        return Type::boolean();
    }

    /**
     * @return PeopleRepository
     */
    private function getRepository()
    {
        return app(StaffRepository::class);
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' => Type::nonNull(Type::int())],
        ];
    }

    public function resolve($root, $args)
    {
        try {

            $this->getRepository()->delete($args['id']);

            return true;

        } catch (\Exception $e) {

            return false;

        }

    }

}