<?php

namespace Modules\Common\GraphQL\Mutations;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use Modules\Common\Entities\Staff;
use Modules\Common\Repositories\Presenters\StaffPresenter;
use Modules\Common\Repositories\StaffRepository;
use Modules\People\Repositories\PeopleRepository;
use Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class StaffCreate extends Mutation
{

    protected $attributes = [
        'name' => 'staffCreate'
    ];

    public function type()
    {
        return GraphQL::type('Staff');
    }

    /**
     * @return PeopleRepository
     */
    private function getRepository()
    {
        return app(StaffRepository::class);
    }

    public function args()
    {
        return [
            'staff' => ['name' => 'staff', 'type' => Type::nonNull(GraphQL::type('StaffCreateInput'))],
        ];
    }

    public function resolve($root, $args)
    {
        $entity = $this->getRepository()
            ->create($args['staff']);

        if($entity && isset($args['staff']['avatar']) && !empty($avatar = $args['staff']['avatar'])) {

            $entity = Staff::find($entity->id);

            // TODO handle this logic via factory class
            $filePath = base_path() . \Storage::disk('temp')->url($avatar['temp_filename']);

            $file = new UploadedFile(
                $filePath,
                $avatar['origin_filename'],
                $avatar['origin_type'],
                $avatar['origin_size']
            );

            // We need to delete the old avatar
            if ($entity->avatar) {
                $entity->avatar->delete();
            }

            $entity->addMedia($file)->toMediaCollection('avatar', 'media');
            $entity->save();

            // Clean up temp file
            Storage::disk('temp')->delete($avatar['temp_filename']);

            // Load updated entity
            $entity = $this->getRepository()
                           ->with(['avatar'])
                           ->setPresenter(new StaffPresenter())
                           ->update( $args['staff'], $entity->id);
        }


        return $entity ?: null;
    }

}