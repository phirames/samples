<?php

namespace Modules\Common\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Modules\Common\Entities\Staff;
use Modules\Common\Policies\StaffPolicy;
use Modules\Faq\Entities\Faq;
use Modules\Faq\Policies\FaqPolicy;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Staff::class => StaffPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }
}