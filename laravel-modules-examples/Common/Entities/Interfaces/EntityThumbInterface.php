<?php


namespace Modules\Common\Entities\Interfaces;

/**
 * Interface EntityThumbInterface
 * @package Modules\Common\Entities\Interfaces
 */
interface EntityThumbInterface
{

    /**
     * Returns an absolute url of the entity thumbnail.
     * If media thumbnail does not exist returns return an url of one of the default thumbnails
     *
     *
     * @param $value
     * @return mixed
     */
    public function getThumbUrlAttribute($value): string;

    /**
     * Returns the entity thumbnail path relative to the root of the site.
     * If media thumbnail does not exist returns return an relative path of one of the default thumbnails
     *
     * @param $value
     * @return string
     */
    public function getThumbSitePathAttribute($value): string;

    /**
     * Returns the default entity thumbnail path relative to the root of the site.
     *
     * @param $value
     * @return string
     */
    public function getDefaultThumbSitePathAttribute($value): string;

}