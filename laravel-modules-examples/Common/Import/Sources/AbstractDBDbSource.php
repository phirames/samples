<?php

namespace Modules\Common\Import\Sources;


use Illuminate\Database\Connection;
use Illuminate\Database\Query\Builder;

abstract class AbstractDBDbSource implements DbSourceInterface
{
    /**
     * @var Connection
     */
    protected $connection;

    /**
     * @var string
     */
    protected $table;

    /**
     * @var string
     */
    protected $defaultTable = 'default';

    /**
     * AbstractDBSource constructor.
     *
     * @param array|null $configs
     * @param string $table
     */
    public function __construct(array $configs = null, string $table = null)
    {
        $this->makeConnection($configs);

        $this->table = $table ?: $this->defaultTable;
    }

    public function makeConnection($options = null)
    {
        // Set the database
        $database = $options['database'];
        $this->database = $database;
        // Figure out the driver and get the default configuration for the driver
        $driver  = isset($options['driver']) ? $options['driver'] : \Config::get("database.default");
        $default = \Config::get("database.connections.$driver");
        // Loop through our default array and update options if we have non-defaults
        foreach($default as $item => $value)
        {
            $default[$item] = isset($options[$item]) ? $options[$item] : $default[$item];
        }
        // Set the temporary configuration
        \Config::set("database.connections.$database", $default);
        // Create the connection
        $this->connection = \DB::connection($database);
    }

    /**
     * @return mixed
     */
    public function getConnection(): Connection
    {
        return $this->connection;
    }

    /**
     * @return Builder
     */
    public function query(): Builder
    {
        return $this->getConnection()->table($this->table);
    }

}