<?php

namespace Modules\Common\Import\Mappers;


interface MapperInterface
{

    public function map($data);

}
