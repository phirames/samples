<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {

            $table->increments('id');

            $table->string('title');

            $table->text('description')->nullable();

            $table->string('domain')->nullable();

            $table->string('type')->nullable();

            $table->jsonb('address')->nullable();

            $table->jsonb('geocode')->nullable();

            $table->jsonb('contacts')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}
