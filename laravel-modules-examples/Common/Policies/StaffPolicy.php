<?php

namespace Modules\Common\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Modules\Common\Entities\Staff;

class StaffPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the people.
     *
     * @param  \App\User $user
     * @param Staff $staff
     * @return mixed
     */
    public function view(User $user, Staff $staff)
    {
        //
    }

    /**
     * Determine whether the user can create people.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the people.
     *
     * @param  \App\User $user
     * @param Staff $staff
     * @return mixed
     */
    public function update(User $user, Staff $staff)
    {
        return $user->hasAnyRole(['super_admin', 'admin']);
    }

    /**
     * Determine whether the user can delete the people.
     *
     * @param  \App\User $user
     * @param Staff $staff
     * @return mixed
     */
    public function delete(User $user, Staff $staff)
    {
        return $user->hasAnyRole(['super_admin', 'admin']);
    }
}
