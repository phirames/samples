<?php


namespace Modules\Common\Services\Interfaces;


use Illuminate\Database\Eloquent\Model;

interface ContentRouteInterface
{

    /**
     * Check if the route exists
     *
     * @param string $routeName
     * @return bool
     */
    public function routeExist(string $routeName): bool;

    /**
     * @param Model $model
     * @return string
     */
    public function routeName(Model $model): string;

}