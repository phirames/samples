<?php


namespace Modules\Common\Services\Interfaces;


use Illuminate\Support\Collection;

interface ContentInterface
{

    /**
     * Search content models and
     *
     * @return Collection
     */
    function contentTypes(): Collection;

}