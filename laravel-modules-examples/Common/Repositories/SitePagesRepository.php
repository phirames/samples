<?php

namespace Modules\Common\Repositories;


use App\Repositories\BaseAppRepository;
use Modules\Common\Entities\SitePage;

class SitePagesRepository extends BaseAppRepository
{
    public function model()
    {
        return SitePage ::class;
    }
}
