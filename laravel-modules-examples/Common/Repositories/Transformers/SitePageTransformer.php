<?php

namespace Modules\Common\Repositories\Transformers;


use App\Repositories\Transformers\EloquentTransformer;
use App\Repositories\Transformers\MediaFileTransformer;
use Modules\Common\Entities\SitePage as Entity;

class SitePageTransformer extends EloquentTransformer
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'files',
        'cover'
    ];

    public function transform(Entity $entity)
    {
        return [
            'id'         => (int) $entity->id,
            'title'      => $entity->title,
            'summary'    => $entity->summary,
            'body'       => $entity->body,
            'route'      => $entity->route(),
            'created_at' => $entity->created_at ? $entity->created_at->toDateTimeString() : null,
            'updated_at' => $entity->updated_at ? $entity->updated_at->toDateTimeString() : null,
            'deleted_at' => $entity->deleted_at ? $entity->deleted_at->toDateTimeString() : null,
        ];
    }

    /**
     * Include images collection
     */
    public function includeFiles(Entity $entity)
    {
        $files = $this->getRelationAsCollection($entity, 'files');

        return $this->collection($files, new MediaFileTransformer());
    }

    public function includeCover(Entity $entity)
    {
        $cover = $this->getRelationAsItem($entity, 'cover');

        return $cover ? $this->item($cover, new MediaFileTransformer()) : null;
    }
}