<?php

Route::group(['middleware' => 'web', 'prefix' => 'letterbooks', 'namespace' => 'Modules\Books\Http\Controllers'], function()
{
    Route::get('/data', 'BooksController@dataList')->name('books.data.list');
    Route::get('/{id}/data', 'BooksController@dataItem')->name('books.data.item');

    Route::get('/', 'BooksController@index')->name('books.index');
    Route::get('/{id}', 'BooksController@show')->name('books.item');
});

Route::group(
    ['middleware' => 'auth:api', 'prefix' => '/api/v1/letterbooks', 'namespace' => 'Modules\Books\Http\Controllers\Api'],
    function() {
        Route::get('/', 'ApiController@index')->name('api.books.index');
        Route::get('/{id}', 'ApiController@show')->name('api.books.show');
        Route::post('/', 'ApiController@store')->name('api.books.create');
        Route::post('/{id}', 'ApiController@update')->name('api.books.update');
        Route::delete('/{id}', 'ApiController@destroy')->name('api.books.delete');

        // Pdf
        Route::post('/{id}/pdf', 'ApiController@updatePdf')
            ->name('api.books.pdf.update');
        Route::delete('/{id}/pdf', 'ApiController@destroyPdf')
            ->name('api.books.pdf.delete');

        // Pdf Text
        Route::post('/{id}/pdf-text', 'ApiController@updatePdfText')
            ->name('api.books.pdf_text.update');
        Route::delete('/{id}/pdf-text', 'ApiController@destroyPdfText')
            ->name('api.books.pdf_text.delete');
    }
);