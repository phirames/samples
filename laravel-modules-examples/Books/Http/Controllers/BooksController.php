<?php

namespace Modules\Books\Http\Controllers;

use App\Repositories\Criteria\RelationsRequestCriteria;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Books\Entities\Book;
use Modules\Books\Repositories\BookRepository;
use Modules\Books\Repositories\Presenters\BookPresenter;
use Modules\Flowpaper\Widgets\PdfViewer;

class BooksController extends Controller
{
    /**
     * @var BookRepository
     */
    protected $repo;

    /**
     * BooksController constructor.
     * @param BookRepository $repository
     */
    public function __construct(BookRepository $repository)
    {
        $this->repo = $repository;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('books::index');
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(int $id)
    {
        $book = Book::findOrFail($id);

        $pdfPath = null;
        $pdfUrl = null;

        if ($pdf = $book->pdf) {
            $pdfPath = $pdf->getPath();
            $pdfUrl = $pdf->getUrl();
        }

        $pdfTextPath = null;
        $pdfTextUrl = null;
        if ($pdfTxt = $book->pdf_txt) {
            $pdfTextPath = $pdfTxt->getPath();
            $pdfTextUrl = $pdfTxt->getUrl();
        }

        return view('books::show', [
            'book' => $book,
            'pdf_path' => $pdfPath,
            'pdf_url' => $pdfUrl,
            'pdf_text_path' => $pdfTextPath,
            'pdf_text_url' => $pdfTextUrl,
        ]);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function dataList(Request $request)
    {
        $relations = $request->get('with', []);
        $limit = $request->get('limit', 25);

        if ($searchString = $request->get('search_string')) {

            $entities = $this->repo
                ->load($relations)
                ->setPresenter($this->getPresenter())
                ->paginatedSearch($searchString, function($client, $query, $params) {
                    $params['body']['query'] = [
                        'multi_match' => [
                            'query'  => $query,
                            'fields' => ['title']
                        ],
                    ];
                    return $client->search($params);
                }, $limit);

        } else {

            $entities = $this->repo
                ->pushCriteria(new RelationsRequestCriteria())
                ->setPresenter($this->getPresenter())
                ->paginate($limit);
        }

        return \Response::json($entities, 206);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     * @throws \Exception
     */
    public function dataItem(Request $request, int $id)
    {
        $location = $this->repo
            ->with($relations = $request->get('with', []))
            ->setPresenter($this->getPresenter())
            ->find($id);

        return \Response::json($location, 200);
    }

    protected function getPresenter()
    {
        return new BookPresenter();
    }

}
