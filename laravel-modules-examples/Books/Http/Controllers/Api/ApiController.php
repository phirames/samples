<?php

namespace Modules\Books\Http\Controllers\Api;

use App\Repositories\Criteria\RelationsRequestCriteria;
use App\Repositories\Criteria\SortById;
use App\Repositories\Presenters\MediaPresenter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Books\Entities\Book as Entity;
use Modules\Books\Repositories\BookRepository;
use Modules\Books\Repositories\Presenters\BookPresenter;

class ApiController extends Controller
{
    /**
     * @var BookRepository
     */
    private $repo;

    public function __construct(BookRepository $repository)
    {
        $this->repo = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        if ($searchString = $request->get('search_string')) {

            $entities = $this->repo
                ->load($request->get('with', []))
                ->setPresenter($this->getPresenter())
                ->paginatedSearch($searchString, function($client, $query, $params) {
                    $params['body']['query'] = [
                        'multi_match' => [
                            'query'  => $query,
                            'fields' => ['title']
                        ],
                    ];
                    return $client->search($params);
                });

        } else {

            $entities = $this->repo
                ->pushCriteria(new RelationsRequestCriteria())
                ->pushCriteria(new SortById())
                ->setPresenter($this->getPresenter())
                ->paginate();

        }

        return response()->json($entities, 206);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('books::create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'    => 'required|max:255',
            'body'     => 'required',
            'date'     => 'nullable|date_format:Y-m-d',
            'pdf'      => 'file:pdf|max:204800',
            'pdf_text' => 'file:pdf|max:204800',
            'with'     => 'array',
            'references' => 'json',
            'terms' => 'json'
        ]);

        $book_date = $request->all();
        $book_date['date'] = $request->get('date') ?: null;

        $book = Entity::create($book_date);

        if ($request->file('pdf')) {
            $book->addMediaFromRequest('pdf')
                ->toMediaCollection(Entity::PDF_MEDIA_COLLECTION_NAME);
        }

        if ($request->file('pdf_text')) {
            $book->addMediaFromRequest('pdf_text')
                ->toMediaCollection(Entity::PDF_TEXT_MEDIA_COLLECTION_NAME);
        }

        $book->syncRefsRequest($request);

        // Update article terms
        $book->syncTermsRequest($request);

        $book->load($request->get('with', []));

        return response()->json($this->getPresenter()->present($book), 201);
    }

    /**
     * Show the specified resource.
     *
     * @param  Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function show(Request $request, int $id)
    {
        $book = $this->repo
            ->with($request->get('with', []))
            ->setPresenter($this->getPresenter())
            ->find($id);

        return response()->json($book, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        return view('books::edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
            'title' => 'required|max:255',
            'body' => 'required',
            'date' => 'nullable|date_format:Y-m-d',
            'with' => 'array',
            'references' => 'json',
            'terms' => 'json'
        ]);

        $book_date = $request->all();
        $book_date['date'] = $request->get('date') ?: null;

        $book = $this->repo->find($id);

        $this->authorize('update', $book);

        $book->update($book_date);

        // Update article references
        $book->syncRefsRequest($request);

        // Update article terms
        $book->syncTermsRequest($request);

        $book->load($request->get('with', []));

        return response()->json($this->getPresenter()->present($book), 200);
    }

    /**
     * Update book pdf file
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function updatePdf(Request $request, int $id)
    {
        // Make sure new file is loaded
        $request->validate([
            'pdf' => 'file:pdf|max:204800',
        ]);

        $entity = $this->repo
            ->find($id);

        $this->authorize('update', $entity);

        // Delete pdf if exists
        if ($pdf = $entity->pdf) {
            $pdf->delete();
        }

        //Add new dpf file
        $entity->addMediaFromRequest('pdf')
            ->toMediaCollection(Entity::PDF_MEDIA_COLLECTION_NAME);

        $entity->load('pdf');

        return response()->json((new MediaPresenter())->present($entity->pdf), 201);
    }

    /**
     * Destroy book pdf file
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroyPdf(int $id)
    {
        $entity = $this->repo
            ->find($id);

        $this->authorize('delete', $entity);

        // Delete pdf file if exist
        if ($pdf = $entity->pdf) {
            return response()->json($pdf->delete(), 204);
        }

        return response()->json(false, 204);
    }

    /**
     * Update book pdf text file
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function updatePdfText(Request $request, int $id)
    {
        // Make sure new file is loaded
        $request->validate([
            'pdf_text' => 'file:pdf|max:204800',
        ]);

        $entity = $this->repo
            ->find($id);

        $this->authorize('update', $entity);

        // Delete pdf if exists
        if ($pdf_text = $entity->pdf_text) {
            $pdf_text->delete();
        }

        //Add new dpf file
        $entity->addMediaFromRequest('pdf_text')
            ->toMediaCollection(Entity::PDF_TEXT_MEDIA_COLLECTION_NAME);

        $entity->load('pdf_text');

        return response()->json((new MediaPresenter())->present($entity->pdf_text), 201);
    }

    /**
     * Destroy book pdf text file
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroyPdfText(int $id)
    {
        $entity = $this->repo
            ->find($id);

        $this->authorize('delete', $entity);

        // Delete pdf file if exist
        if ($pdf_text = $entity->pdf_text) {
            return response()->json($pdf_text->delete(), 204);
        }

        return response()->json(false, 204);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(int $id)
    {
        $book = $this->repo->find($id);

        $this->authorize('delete', $book);

        $book->delete();

        return response()->json($book, 204);
    }

    /**
     * @return BookPresenter
     * @throws \Exception
     */
    public function getPresenter()
    {
        return new BookPresenter();
    }
}
