<?php

namespace Modules\Books\Entities;

use App\Entities\Interfaces\GqlEntity;
use App\Entities\Traits\EsSearchable;
use Illuminate\Database\Eloquent\Model;
use Modules\Books\Es\Documents\BookDoc;
use Modules\Books\GraphQL\Types\LetterBookType;
use App\Entities\Interfaces\ContentInterface;
use Modules\Common\Entities\Interfaces\EntityThumbInterface;
use App\Entities\Interfaces\ReferenceInterface;
use App\Entities\Traits\ContentTrait;
use App\Entities\Traits\References;
use Modules\Common\Entities\Traits\ThumbnailTrait;
use Modules\Media\Entities\Interfaces\MediaLibraryInterface;
use Modules\Media\Entities\MediaTrait;
use Modules\Taxonomy\Entities\Traits\TaxonomyTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Media;

class Book extends Model implements
    HasMediaConversions,
    EntityThumbInterface,
    ReferenceInterface,
    MediaLibraryInterface,
    ContentInterface,
    GqlEntity
{
    use MediaTrait, ThumbnailTrait, References, ContentTrait, EsSearchable, TaxonomyTrait;

    const PDF_MEDIA_COLLECTION_NAME = 'medium_resolution';
    const PDF_TEXT_MEDIA_COLLECTION_NAME = 'text_copy';

    protected $fillable = [
        'title', 'body', 'date'
    ];

    protected $casts = [
        'metadata' => 'object',
    ];

    protected $table = 'content_books';

    protected $mappingName = 'book';

    protected $contentTypeName = 'book';

    protected $routeName = 'books.item';

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(150)
            ->sharpen(10);
    }

    /**
     * Get snippet thumbnail url
     *
     * @return string
     */
    public function getThumbSitePathAttribute($value = ''): string
    {

        $thumb = '';
        if ($this->pdf) {
            $thumb = $this->pdf->getPath('thumb');
        }elseif ($this->txt) {
            $thumb = $this->txt->getPath('thumb');
        }
        $thumb = str_replace(base_path('storage'), '', $thumb);

        return $thumb ?: $this->getDefaultThumbSitePathAttribute($value);
    }

    /**
     * Get pdf file relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function pdf()
    {
        return $this->morphOne(config('medialibrary.media_model'), 'model')
            ->where('collection_name', '=', self::PDF_MEDIA_COLLECTION_NAME);
    }

    /**
     * Get pdf file relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function pdf_text()
    {
        return $this->morphOne(config('medialibrary.media_model'), 'model')
            ->where('collection_name', '=', self::PDF_TEXT_MEDIA_COLLECTION_NAME);
    }

    public function getGqlType(): string
    {
        return (new LetterBookType())->name;
    }

    public function toSearchableArray()
    {
        return (new BookDoc($this))->source();
    }
}
