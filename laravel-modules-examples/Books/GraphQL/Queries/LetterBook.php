<?php

namespace Modules\Books\GraphQL\Queries;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use Modules\Books\GraphQL\Mappers\LetterBookEntityMapper;
use Modules\Books\Entities\Book;

class LetterBook extends Query
{
    protected $attributes = [
        'name' => 'letterBook'
    ];

    public function type()
    {
        return GraphQL::type('LetterBook');
    }

    public function args()
    {
        return [
            'id' => ['name' => 'id', 'type' =>  Type::nonNull(Type::int())],
        ];
    }

    public function resolve($root, $args)
    {
        $media  = Book::where('id' , $args['id'])->get()->first();
        return (new LetterBookEntityMapper($media))->toArray();
    }
}
