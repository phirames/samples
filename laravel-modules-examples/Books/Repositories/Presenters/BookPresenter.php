<?php

namespace Modules\Books\Repositories\Presenters;

use Modules\Books\Repositories\Transformers\BookTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class BookPresenter extends FractalPresenter
{

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BookTransformer();
    }
}
