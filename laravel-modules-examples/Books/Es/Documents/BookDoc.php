<?php

namespace Modules\Books\Es\Documents;


use App\Es\Documents\EgwEloquentDocModel;
use Modules\Books\Entities\Book;
use Modules\Books\Es\MappingTypes\BookMappingType;
use Modules\Search\Search\Mappings\PdfDocMediaResource;
use Modules\Search\Search\Mappings\Snippet;

class BookDoc extends EgwEloquentDocModel
{

    public function __construct(Book $modelEntry)
    {
        parent::__construct($modelEntry);
    }

    public static function modelType(): string
    {
        return Book::class;
    }

    public static function mappingType(): string
    {
        return BookMappingType::name();
    }

    public function source(): array
    {
        return [
            'title' => $this->modelEntry->title,
            'suggest' => $this->modelEntry->title,

            'body' => $this->modelEntry->body,

            'date' => $this->modelEntry->date,

            'snippet' => $this->snippet(),

            'resources' => $this->resources(),
            'media_resources' => $this->mediaResources(),

            'created_at' => $this->modelEntry->created_at ? $this->modelEntry->created_at->toDateTimeString() : null,
            'updated_at' => $this->modelEntry->updated_at ? $this->modelEntry->updated_at->toDateTimeString() : null,
            'deleted_at' => $this->modelEntry->deleted_at ? $this->modelEntry->deleted_at->toDateTimeString() : null,
        ];

    }

    protected function resources(): array
    {
        $resources = [];
        if ($this->modelEntry->pdf) {
            $resources['pdf'][] = str_replace(base_path('storage'), '', $this->modelEntry->pdf->getPath());
        }
        if ($this->modelEntry->txt) {
            $resources['pdf'][] = str_replace(base_path('storage'), '', $this->modelEntry->txt->getPath());;
        }
        return $resources;
    }

    protected function mediaResources(): array
    {
        $mediaResources = [];

        if ($this->modelEntry->pdf) {
            $path = str_replace(base_path('storage'), '',  $this->modelEntry->pdf->getPath());
            $mediaResources[] = new PdfDocMediaResource($this->modelEntry->pdf->name, $path);
        }

        if ($this->modelEntry->txt) {
            $path = str_replace(base_path('storage'), '',  $this->modelEntry->txt->getPath());
            $mediaResources[] = new PdfDocMediaResource($this->modelEntry->txt->name, $path);
        }


        return $mediaResources;
    }



    /**
     * Create search snippet
     *
     * @return Snippet
     */
    protected function snippet(): Snippet
    {
        $title = $this->modelEntry->title;
        $body = str_limit(strip_tags($this->modelEntry->body), 255);
        $url = route('books.item', ['id' => $this->modelEntry->id], false);
        $thumbUrl = $this->modelEntry->thumb_site_path;
        $type = 'Book';

        $snippet = new Snippet($title, $body, $url, $thumbUrl, $type);

        return $snippet;
    }

}