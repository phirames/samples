$(document).ready(function() {
	// Functional of scrollTop btn and sticky orange menu
	function scrollBehaviour(){
		var orangeMenu = $('.egw-main-menu'),
	        orangeMenuOffsetTop = $('header')[0].getBoundingClientRect().bottom,
	        footerOffsetTop = $('#footer')[0].getBoundingClientRect().top;

	    if (orangeMenuOffsetTop <= 51) {
	        orangeMenu.addClass('stick');
	        $('.main-content').addClass('stickedOrangeMenu');
	    } else {
	        orangeMenu.removeClass('stick');
	        $('.main-content').removeClass('stickedOrangeMenu');
	    }

	    if (document.body.scrollTop > 60 || document.documentElement.scrollTop > 60) {
	        $('.top-scroller').addClass('visible');
	        $('#mainNav').addClass('affix');
	    } else {
	        $('.top-scroller').removeClass('visible');
	        $('#mainNav').removeClass('affix');
	        return false;
	    }

	    if (footerOffsetTop <= $(window).height()) {
	    	$('.top-scroller').css('bottom', $(window).height() - footerOffsetTop + 50);
	    } else {
	    	$('.top-scroller').css('bottom', '');
	    }
	};
	scrollBehaviour();
	$(document).on('scroll', function(){
	    scrollBehaviour();
	});

	$('.top-scroller').click(function(){
		$('html, body').animate({
			scrollTop:0
		},500);
	});
	// ---------


	// Search in header
	$(".main-navbar .navbar-form .btn").click(function(e){
		e.preventDefault();

		window.location.href=window.location.origin + "/search#searchquery-" + $(this).siblings('input[type="text"]').val();
	});
	$(".main-navbar .navbar-form input[type='text']").keypress(function(event) {
		if(event.keyCode == 13){
			event.preventDefault();
			$(".main-navbar .navbar-form a.btn-default-sm").click();
		}
	});
	// ---------

	// Search input in header
	$('.main-navbar__clear-search').on('mousedown', function(event) {
	    event.preventDefault();
	}).on('click', function() {
		$('.main-navbar .navbar-form input.form-control')[0].value = "";
	});
	$('.main-navbar .navbar-form .btn').on('mousedown', function(event) {
	    event.preventDefault();
	}).on('click', function() {
	    $('.main-navbar .navbar-form').submit();
	});
	$('.main-navbar .navbar-form input[type="text"]').focus(function(){
		$('.main-navbar .navbar-form').addClass('focus');
	}).blur(function(){
		$('.main-navbar .navbar-form').removeClass('focus');
	});
	// ---------


	// Social links in header
	$('.main-navbar__share > a').click(function(e){
		e.preventDefault();
		if($(this).parent().hasClass('active')){
			$(this).parent().removeClass('active');
			$('.main-navbar__share-items-wrap').fadeOut();
		} else {
			$(this).parent().addClass('active');
			$('.main-navbar__share-items-wrap').fadeIn();
		}
	});
	// ---------
});
