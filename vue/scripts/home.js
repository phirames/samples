import Swiper from 'swiper'
import 'jquery-lazyload'

$(document).ready(function($) {
  // Home slider
  var mySwiper = new Swiper ('.swiper-container', {
    loop: true,
    nextButton: '.homepage-header__qoute-slider-next',
    prevButton: '.homepage-header__qoute-slider-prev'
  })
  // ---------

  // Lazy loading for images
  $("img.lazy").lazyload({});
  // ---------

  // Links for sections
	$('.home-nav__item a').click(function(event) {
		event.preventDefault();

		var sectionId = $(this).attr('href'),
			sectionOffsetTop;

		if(sectionId == '#') return false;

		if(window.innerWidth < 1024){
			sectionOffsetTop = $(sectionId).offset().top - $('.main-navbar').height() - 20
		}  else {
			sectionOffsetTop = $(sectionId).offset().top - $('.main-navbar').height() - $('#egw-main-menu').height() - 20
		}

		$('html, body').animate({
			scrollTop: sectionOffsetTop
		},500);
	});
  // ---------
});
