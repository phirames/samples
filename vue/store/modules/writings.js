

// initial state
const state = {
    content: {
        mirrors: [
            "http://media4.egwwritings.org",
            "https://egwwritings-a.akamaihd.net",
            "http://egwwritings-a.akamaihd.net",
            "https://media3.egwwritings.org",
            "http://media3.egwwritings.org",
            "https://media2.egwwritings.org",
            "http://media2.egwwritings.org"
        ]
    }
}

// getters
const getters = {
}

// actions
const actions = {
}

// mutations
const mutations = {
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}